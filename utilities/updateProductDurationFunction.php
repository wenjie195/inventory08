<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/RenewTimeline.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function renewProduct($conn,$uid,$productUid,$productName,$productCode,$previousExDate,$duration,$expiredDate)
{
     if(insertDynamicData($conn,"renew_timeline",array("uid","product_uid","product_name","product_code","previous_exdate","duration","expired_date"),
          array($uid,$productUid,$productName,$productCode,$previousExDate,$duration,$expiredDate),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $productName = rewrite($_POST['product_name']);
     $productUid = rewrite($_POST['product_uid']);

     $productDetails = getAddOnProduct($conn," WHERE uid = ? ",array("uid"),array($productUid),"s");
     $productName = $productDetails[0]->getProductName();
     $productCode = $productDetails[0]->getProductCode();
     $previousExDate = $productDetails[0]->getExpiredDate();

     $duration = rewrite($_POST['duration']);

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz));
     $dt->setTimestamp($timestamp);
     $currentTime = $dt->format('Y-m-d');

     $Date1 = $currentTime;
     $date = new DateTime($Date1);
     $additional = $duration;
     $date->modify($additional);
     $expiredDate = $date->format('Y-m-d');

     // $maintenance = [];
     // $maintenance = " ";
     $maintenance = "Renew";
     
     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($expiredDate)
          {
               array_push($tableName,"expired_date");
               array_push($tableValue,$expiredDate);
               $stringType .=  "s";
          }
          // if($maintenance)
          if(!$maintenance || $maintenance)
          {
               array_push($tableName,"maintenance");
               array_push($tableValue,$maintenance);
               $stringType .=  "s";
          }
          if($duration)
          {
               array_push($tableName,"duration");
               array_push($tableValue,$duration);
               $stringType .=  "s";
          }

          array_push($tableValue,$productUid);
          $stringType .=  "s";
          $updateQuantity = updateDynamicData($conn,"addon_product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateQuantity)
          {
               // header('Location: ../adminDashboard.php');

               if(renewProduct($conn,$uid,$productUid,$productName,$productCode,$previousExDate,$duration,$expiredDate))
               {
                    // echo "success";
                    header('Location: ../adminDashboard.php');
               }
               else
               {
                    echo "fail";
               }

          }
          else
          {
               echo "fail fail";
          }
     }
     else
     {
          echo "ERROR !!";
     }
}
else 
{
     header('Location: ../index.php');
}
?>