<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addProduct($conn,$uid,$productName,$productCode,$category,$currentQuantity,$cost,$averageCost,$partNumber,$duration,$expiredDate)
{
     if(insertDynamicData($conn,"addon_product",array("uid","product_name","product_code","category","quantity","cost","average_cost","part_number","duration","expired_date"),
          array($uid,$productName,$productCode,$category,$currentQuantity,$cost,$averageCost,$partNumber,$duration,$expiredDate),"ssssssdsss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $productName = rewrite($_POST['product_name']);
     $category = rewrite($_POST['update_category']);
     $productCode = rewrite($_POST['product_code']);

     $productDetails = getProduct($conn," WHERE product_name = ? ",array("product_name"),array($productName),"s");
     $currentQuantity = $productDetails[0]->getQuantity();

     $updateQuantity = rewrite($_POST['update_quantity']);
     $quantity = $currentQuantity + $updateQuantity;

     $partNumber = rewrite($_POST['update_part_number']);
     $description = rewrite($_POST['update_description']);
     $cost = rewrite($_POST['update_cost']);
     $averageCost = ($cost / $quantity);
     $location = rewrite($_POST['update_location']);

     $productUid = rewrite($_POST['product_uid']);

     $duration = rewrite($_POST['duration']);

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $currentTime = $dt->format('Y-m-d');

     // echo $Date1 = '2010-09-17';
     $Date1 = $currentTime;
     $date = new DateTime($Date1);
     $additional = $duration;
     // echo $additional = "10 days";
     // $date->modify('+1 day');
     $date->modify($additional);
     $expiredDate = $date->format('Y-m-d');
     
     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($productName)
          {
               array_push($tableName,"product_name");
               array_push($tableValue,$productName);
               $stringType .=  "s";
          }
          if($category)
          {
               array_push($tableName,"category");
               array_push($tableValue,$category);
               $stringType .=  "s";
          }
          if($productCode)
          {
               array_push($tableName,"product_code");
               array_push($tableValue,$productCode);
               $stringType .=  "s";
          }
          if($quantity)
          {
               array_push($tableName,"quantity");
               array_push($tableValue,$quantity);
               $stringType .=  "s";
          }
          if($partNumber)
          {
               array_push($tableName,"part_number");
               array_push($tableValue,$partNumber);
               $stringType .=  "s";
          }
          if($description)
          {
               array_push($tableName,"description");
               array_push($tableValue,$description);
               $stringType .=  "s";
          }
          if($cost)
          {
               array_push($tableName,"cost");
               array_push($tableValue,$cost);
               $stringType .=  "s";
          }
          if($location)
          {
               array_push($tableName,"location");
               array_push($tableValue,$location);
               $stringType .=  "s";
          }
          array_push($tableValue,$productUid);
          $stringType .=  "s";
          $updateQuantity = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateQuantity)
          {
               // header('Location: ../adminDashboard.php');

               if(addProduct($conn,$uid,$productName,$productCode,$category,$currentQuantity,$cost,$averageCost,$partNumber,$duration,$expiredDate))
               {
                    // echo "success";
                    header('Location: ../adminDashboard.php');
               }
               else
               {
                    echo "fail";
               }

          }
          else
          {
               echo "fail fail";
          }
     }
     else
     {
          echo "ERROR !!";
     }
}
else 
{
     header('Location: ../index.php');
}
?>