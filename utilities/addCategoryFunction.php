<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Category.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addCategory($conn,$uid,$categoryName,$status)
{
     if(insertDynamicData($conn,"category",array("uid","name","status"),
          array($uid,$categoryName,$status),"sss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $categoryName = rewrite($_POST['category_name']);
     $status = "Available";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";

     $allCategory = getCategory($conn," WHERE name = ? ",array("name"),array($_POST['category_name']),"s");
     $existingCategory = $allCategory[0];

     if (!$existingCategory)
     {
          if(addCategory($conn,$uid,$categoryName,$status))
          {
               header('Location: ../adminAddCategory.php');
          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "category existed !! pls change recheck";
     }
}
else 
{
     header('Location: ../index.php');
}
?>