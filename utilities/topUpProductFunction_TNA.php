<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Name.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$description,$cost)
{
     if(insertDynamicData($conn,"product",array("uid","product_name","category","product_code","quantity","description","cost"),
          array($uid,$productName,$category,$productCode,$quantity,$description,$cost),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $productName = rewrite($_POST['product_name']);
     $category = rewrite($_POST['category']);

     $productDetails = getName($conn," WHERE name = ? ",array("name"),array($productName),"s");
     $productCode = $productDetails[0]->getProductCode();

     // $productCode = rewrite($_POST['product_code']);
     // $quantity = rewrite($_POST['quantity']);
     $quantity = " ";

     $description = rewrite($_POST['item_description']);
     $cost = rewrite($_POST['cost']);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";

     if(addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$description,$cost))
     {
          echo "success";
     }
     else
     {
          echo "fail";
     }

}
else 
{
     header('Location: ../index.php');
}
?>