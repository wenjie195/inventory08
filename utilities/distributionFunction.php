<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Name.php';
require_once dirname(__FILE__) . '/../classes/Excel.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function distributeProduct($conn,$uid,$name,$department,$quantity,$productName,$productCode)
{
     if(insertDynamicData($conn,"distribution_list",array("uid","name","department","quantity_received","product_name","product_code"),
          array($uid,$name,$department,$quantity,$productName,$productCode),"ssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $productOrderUid = rewrite($_POST['product_order_uid']);

     $name = rewrite($_POST['name']);
     $department = rewrite($_POST['department']);
     $quantity = rewrite($_POST['quantity']);
     $productName = rewrite($_POST['product_name']);
     $productCode = rewrite($_POST['product_code']);
     $productQuantity = rewrite($_POST['current_quantity']);

     $newQuantity = $productQuantity - $quantity;
     $status = "Solved";

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($newQuantity)
          {
               array_push($tableName,"quantity");
               array_push($tableValue,$newQuantity);
               $stringType .=  "s";
          }
          array_push($tableValue,$productName);
          $stringType .=  "s";
          $updateQuantity = updateDynamicData($conn,"product"," WHERE product_name = ? ",$tableName,$tableValue,$stringType);
          if($updateQuantity)
          {
               if(isset($_POST['submit']))
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    // //echo "save to database";
                    if($status)
                    {
                         array_push($tableName,"status");
                         array_push($tableValue,$status);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$productOrderUid);
                    $stringType .=  "s";
                    $updateStatus = updateDynamicData($conn,"excel"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateStatus)
                    {
                         if(distributeProduct($conn,$uid,$name,$department,$quantity,$productName,$productCode))
                         {
                              // echo "success";
                              header('Location: ../adminViewOrderDetails.php');
                         }
                         else
                         {
                              echo "fail";
                         }
                    }
                    else
                    {
                         echo "fail fail AA";
                    }
               }
               else
               {
                    echo "ERROR AA !!";
               }
          }
          else
          {
               echo "fail fail BB";
          }
     }
     else
     {
          echo "ERROR BB !!";
     }

     // if(addProduct($conn,$uid,$name,$department,$quantity,$productName,$productCode))
     // {
     //      echo "success";
     // }
     // else
     // {
     //      echo "fail";
     // }

}
else 
{
     header('Location: ../index.php');
}
?>