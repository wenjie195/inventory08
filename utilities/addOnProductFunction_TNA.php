<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Name.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$cost,$repointOrder,$location)
{
     if(insertDynamicData($conn,"addon_product",array("uid","product_name","category","product_code","quantity","cost","brand","location"),
          array($uid,$productName,$category,$productCode,$quantity,$cost,$repointOrder,$location),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $productName = rewrite($_POST['product_name']);
     $category = rewrite($_POST['category']);

     $productDetails = getName($conn," WHERE name = ? ",array("name"),array($productName),"s");
     $productCode = $productDetails[0]->getProductCode();

     $quantity = rewrite($_POST['update_quantity']);

     $productData = getProduct($conn," WHERE product_name = ? ",array("product_name"),array($productName),"s");
     $currentQuantity = $productData[0]->getQuantity();

     $newQuantity = $currentQuantity + $quantity;

     $cost = rewrite($_POST['update_cost']);

     $repointOrder = rewrite($_POST['repoint_order']);
     $location = rewrite($_POST['location']);
     
     $productUid = rewrite($_POST['product_uid']);
     
     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($newQuantity)
          {
               array_push($tableName,"quantity");
               array_push($tableValue,$newQuantity);
               $stringType .=  "s";
          }
          array_push($tableValue,$productName);
          $stringType .=  "s";
          $updateQuantity = updateDynamicData($conn,"product"," WHERE product_name = ? ",$tableName,$tableValue,$stringType);
          if($updateQuantity)
          {
               if(addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$cost,$repointOrder,$location))
               {
                    // echo "success";
                    header('Location: ../adminDashboard.php');
               }
               else
               {
                    echo "fail";
               }
          }
          else
          {
               echo "fail fail";
          }
     }
     else
     {
          echo "ERROR !!";
     }

     // if(addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$cost,$repointOrder,$location))
     // {
     //      // echo "success";
     //      header('Location: ../adminDashboard.php');
     // }
     // else
     // {
     //      echo "fail";
     // }

}
else 
{
     header('Location: ../index.php');
}
?>