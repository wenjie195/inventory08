<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Name.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addProductName($conn,$uid,$productName,$productCode,$status)
{
     if(insertDynamicData($conn,"name",array("uid","name","product_code","status"),
          array($uid,$productName,$productCode,$status),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $productName = rewrite($_POST['product_name']);
     $productCode = rewrite($_POST['product_code']);
     $status = "Available";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";

     $allName = getName($conn," WHERE name = ? ",array("name"),array($_POST['product_name']),"s");
     $existingName = $allName[0];

     $allProductCode = getName($conn," WHERE product_code = ? ",array("product_code"),array($_POST['product_code']),"s");
     $existingProductCode = $allProductCode[0];

     if (!$existingName)
     {
          if (!$existingProductCode)
          {
               if(addProductName($conn,$uid,$productName,$productCode,$status))
               {
                    echo "success";
               }
               else
               {
                    echo "fail";
               }
          }
          else
          {
               echo "product code existed !! pls change new code";
          }
     }
     else
     {
          echo "name existed !! pls change new name";
     }

}
else 
{
     header('Location: ../index.php');
}

?>