<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/../classes/Name.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$partNumber,$description,$imageOne,$cost,$location)
{
     if(insertDynamicData($conn,"product",array("uid","product_name","category","product_code","quantity","part_number","description","image","cost","location"),
          array($uid,$productName,$category,$productCode,$quantity,$partNumber,$description,$imageOne,$cost,$location),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function topUpProduct($conn,$newUid,$productName,$productCode,$partNumber,$category,$quantity,$description,$cost,$averageCost,$duration,$expiredDate)
{
     if(insertDynamicData($conn,"addon_product",array("uid","product_name","product_code","part_number","category","quantity","brand","cost","average_cost","duration","expired_date"),
          array($newUid,$productName,$productCode,$partNumber,$category,$quantity,$description,$cost,$averageCost,$duration,$expiredDate),"ssssssssdss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// function topUpProduct($conn,$newUid,$productName,$productCode,$partNumber,$category,$quantity,$description,$cost,$averageCost,$duration)
// {
//      if(insertDynamicData($conn,"addon_product",array("uid","product_name","product_code","part_number","category","quantity","brand","cost","average_cost","duration"),
//           array($newUid,$productName,$productCode,$partNumber,$category,$quantity,$description,$cost,$averageCost,$duration),"ssssssssss") === null)
//      {
//           echo "ggaa";
//      }
//      else{    }
//      return true;
// }


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $newUid = md5(uniqid());

     $productName = rewrite($_POST['product_name']);
     $category = rewrite($_POST['category']);
     $productCode = rewrite($_POST['product_code']);
     $quantity = rewrite($_POST['quantity']);
     $partNumber = rewrite($_POST['part_number']);
     $description = rewrite($_POST['item_description']);

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $cost = rewrite($_POST['cost']);

     $averageCost = ($cost / $quantity);

     $location = rewrite($_POST['location']);

     $duration = rewrite($_POST['duration']);

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $currentTime = $dt->format('Y-m-d');

     // echo $Date1 = '2010-09-17';
     $Date1 = $currentTime;
     $date = new DateTime($Date1);
     $additional = $duration;
     // echo $additional = "10 days";
     // $date->modify('+1 day');
     $date->modify($additional);
     $expiredDate = $date->format('Y-m-d');

     // date_add($date, date_interval_create_from_date_string('100 days'));
     // date_add($date, date_interval_create_from_date_string('10 months'));
     // date_add($date, date_interval_create_from_date_string($duration));
     // $newdate = $time + $duration;
     // $timeline = date_format($date, 'Y-m-d');


     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productName."<br>";
     // echo $category."<br>";
     // echo $productCode."<br>";
     // echo $partNumber."<br>";
     // echo $duration."<br>";
     // echo $time."<br>";
     // echo $date."<br>";
     // echo $newdate."<br>";

     $productNameRows = getAddOnProduct($conn," WHERE product_name = ? ",array("product_name"),array($productName),"s");
     $existingProductName = $productNameRows[0];

     $productCodeRows = getAddOnProduct($conn," WHERE product_code = ? ",array("product_code"),array($productCode),"s");
     $existingProductCode = $productCodeRows[0];

     if (!$existingProductName && !$existingProductCode)
     {
          if(addProduct($conn,$uid,$productName,$category,$productCode,$quantity,$partNumber,$description,$imageOne,$cost,$location))
          {
               // echo "success";
               // header('Location: ../adminDashboard.php');
     
               if(topUpProduct($conn,$newUid,$productName,$productCode,$partNumber,$category,$quantity,$description,$cost,$averageCost,$duration,$expiredDate))
               // if(topUpProduct($conn,$newUid,$productName,$productCode,$partNumber,$category,$quantity,$description,$cost,$averageCost,$duration))
               {
                    // echo "success";
                    header('Location: ../adminDashboard.php');
                    // header('Location: ../adminViewInvoice.php');
               }
               else
               {
                    echo "fail";
               }
     
          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "<script>alert('This product already exist !! ');window.location='../adminAddProduct.php'</script>";
     }

}
else 
{
     header('Location: ../index.php');
}
?>