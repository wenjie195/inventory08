<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Excel.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getExcel($conn);
$productDetails = getExcel($conn, "WHERE status = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="BOM List Details | ChiNou IMS" />
    <title>BOM List Details | ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-title open">Requested Item List</h1>

    <div class="clear"></div>

    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Product Code</th>
                    <th>Part Number</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $productDetails[$cnt]->getCategory();?></td>
                            <td><?php echo $productDetails[$cnt]->getProductCode();?></td>
                            <td><?php echo $productDetails[$cnt]->getPartNumber();?></td>
                            <td><?php echo $productDetails[$cnt]->getQuantity();?></td>

                            <td>
                                <form action="adminCheckOrderDetails.php" method="POST">
                                    <input type="hidden" id="product_name" name="product_name" value="<?php echo $productDetails[$cnt]->getProductName() ?>">
                                    <input type="hidden" id="product_code" name="product_code" value="<?php echo $productDetails[$cnt]->getProductCode() ?>">
                                    <input type="hidden" id="product_quantity" name="product_quantity" value="<?php echo $productDetails[$cnt]->getQuantity() ?>">
                                    <button class="clean hover1 img-btn" type="submit" name="product_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                        <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                    </button>
                                </form>
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>

</div>

<style>
.viewbom-li{
	color:#264a9c;
	background-color:white;}
.viewbom-li .hover1a{
	display:none;}
.viewbom-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>