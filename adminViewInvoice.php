<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');

// echo $dateAdded = $productDetails[$cnt]->getDateCreated();
// echo "<br>";
$Date1 = $time;
$date = new DateTime($Date1);
// $additional = $duration;
$additional = "-1 hour";
// $date->modify('+1 day');
$date->modify($additional);
$expiredDate = $date->format('Y-m-d H:i:s');

$conn = connDB();

// $productDetails = getProduct($conn);
// $productDetails = getProduct($conn, "WHERE date_created > '$expiredDate' ");
$productDetails = getAddOnProduct($conn, "WHERE date_created > '$expiredDate' ");

if($productDetails)
{
    $totalBonusOne = 0;
    for ($cnt=0; $cnt <count($productDetails) ; $cnt++)
    {
    $totalBonusOne += $productDetails[$cnt]->getCost();
    }
}
else
{
    $totalBonusOne = 0 ;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $registerMS = rewrite($_POST["register_ms"]);
    $registerAttention = rewrite($_POST["register_attention"]);
}
else 
{   }

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Invoice | ChiNou IMS" />
    <title>Invoice | ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar page1-div">
	<p class="invoice-logo-p text-center"><img src="img/invoice-logo.png" class="invoice-logo" alt="ChiNou Smart Solutions Sdn. Bhd." title="ChiNou Smart Solutions Sdn. Bhd."></p>
    <h3 class="invoice-company-name">ChiNou Smart Solutions Sdn. Bhd.</h3>
    <h3 class="invoice-company-number">Company No.: 201901034835(1344165-T)</h3>
	<h3 class="invoice-text">Invoice</h3>
   
      <p class="ms-p">M/s: <b><?php echo $registerMS;?></b></p>
    


      <p class="ms-p">Attention: <b><?php echo $registerAttention;?></b></p>

    <div class="clear"></div>

    

    <div class="clear"></div>

    <div class="width100">
  
    
        <table class="shipping-table print-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Product Code</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th style="text-align:right">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productName = $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $productDetails[$cnt]->getCategory();?></td>
                            <td><?php echo $productDetails[$cnt]->getProductCode();?></td>
                            <td><?php echo $quantity = $productDetails[$cnt]->getQuantity();?></td>
                            <td>
                              <?php 
                                $price = $productDetails[$cnt]->getCost();
                                echo $totalPrice =  $price / $quantity;
                              ?>
                            </td>
                            <td style="text-align:right">
                              <?php echo $price;?>
                            </td>
                        </tr>

                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>

        <div class="clear"></div>
		<table  class="shipping-table width100">
        	<tr>
            	<td>Grand Total:</td>
                <td style="text-align:right"><b><?php echo $totalBonusOne;?></b></td>
            </tr>
        </table>
        

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn no-print" onclick="window.print()">Generate PDF</button>
		<p class="text-center pointer red-link"><a href="./img/pdf-tutorial.jpg" data-fancybox class="red-link">Tutorial</a></p>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.invoice-li{
	color:#264a9c;
	background-color:white;}
.invoice-li .hover1a{
	display:none;}
.invoice-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>