<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard | ChiNou IMS" />
    <title>Admin Dashboard | ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-title open">Admin Dashboard</h1>

    <div class="clear"></div>

    <div class="big-four-input-container">
      <div class="three-input-div">
        <p class="input-top-p">Product Name</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Product Name" class="tele-four-input tele-input clean">
      </div>

      <div class="three-input-div left-three-input">
        <p class="input-top-p">Category</p>
        <input type="text" id="myInputB" onkeyup="myFunctionB()" placeholder="Category" class="tele-four-input tele-input clean">
      </div>

      <div class="three-input-div">
        <p class="input-top-p">Product Code</p>
        <input type="text" id="myInputC" onkeyup="myFunctionC()" placeholder="Product Code" class="tele-four-input tele-input clean">
      </div>
	</div>

    <div class="width100 shipping-div2">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Product Code</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Location</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Edit</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productName = $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $productDetails[$cnt]->getCategory();?></td>
                            <td><?php echo $productDetails[$cnt]->getProductCode();?></td>
                            <td><?php echo $productDetails[$cnt]->getDescription();?></td>
                            <td>
                              <?php 
                                echo $quantity = $productDetails[$cnt]->getQuantity();
                                if($quantity <= 100)
                                {
                                  echo "<br>";
                                  echo "Low Amount, Please Restock !!";
                                }
                                else
                                {}
                              ?>
                            </td>
                            <td><?php echo $productDetails[$cnt]->getLocation();?></td>

                            <td>
                              <!-- <?php //echo $productDetails[$cnt]->getImage();?> -->
                              <a href="./img/add-product2.png" data-fancybox class="pointer"><img src="img/add-product2.png" class="small-image" ></a>
                            </td>

                            <td>
                              <?php
                                $tz = 'Asia/Kuala_Lumpur';
                                $timestamp = time();
                                $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
                                $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
                                $currentTime = $dt->format('Y-m-d');

                                $conn = connDB();
                                $expiredProduct = getAddOnProduct($conn,"WHERE product_name = ? AND expired_date <= '$currentTime' ", array("product_name") ,array($productName),"s");
                                if($expiredProduct)
                                {   
                                    echo $totalProductList = count($expiredProduct);
                                }
                                else
                                {   echo $totalProductList = 0;   }
                              ?>
                            </td>

                            <td>
                                <!-- <form action="adminAddProduct.php" method="POST"> -->
                                <form action="adminEditProduct.php" method="POST">
                                    <button class="clean hover1 img-btn" type="submit" name="product_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                        <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="adminViewProductDetails.php" method="POST">
                                    <button class="clean hover1 img-btn" type="submit" name="product_name" value="<?php echo $productDetails[$cnt]->getProductName();?>">
                                        <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                        <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                    </button>
                                </form>
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>