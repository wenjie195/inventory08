<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Name.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allName = getName($conn);
$allCategory = getCategory($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Top Up Product| ChiNou IMS" />
    <title>Top Up Product| ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Add Product</h1> 
    <form action="utilities/topUpProductFunction.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Product Name</p>
            <!-- <input class="clean tele-input" type="text" placeholder="Product Name" id="product_name" name="product_name" required>         -->
            <select class="clean tele-input" value="<?php echo $allName[0]->getName();?>" name="product_name" id="product_name" required>
                <option value="">Please Select a Product Name</option>
                <?php
                for ($cnt=0; $cnt <count($allName) ; $cnt++)
                {
                ?>
                    <option value="<?php echo $allName[$cnt]->getName(); ?>"> 
                        <?php echo $allName[$cnt]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>  
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Category</p>
            <!-- <input class="clean tele-input"  type="text" placeholder="Category" id="category" name="category" required>        -->
            <select class="clean tele-input" value="<?php echo $allCategory[0]->getName();?>" name="category" id="category" required>
                <option value="">Please Select a Category</option>
                <?php
                for ($cntAA=0; $cntAA <count($allCategory) ; $cntAA++)
                {
                ?>
                    <option value="<?php echo $allCategory[$cntAA]->getName(); ?>"> 
                        <?php echo $allCategory[$cntAA]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>  
        </div> 

        <div class="clear"></div>

        <!-- <div class="input50-div">
            <p class="input-title-p">Quantity</p>
            <input class="clean tele-input"  type="text" placeholder="Quantity" id="quantity" name="quantity">        
        </div> 

        <div class="clear"></div> -->

        <div class="input50-div">
            <p class="input-title-p">Item Description</p>
            <input class="clean tele-input" type="text" placeholder="Item Description" id="item_description" name="item_description" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Cost</p>
            <input class="clean tele-input"  type="text" placeholder="Cost" id="cost" name="cost" required>        
        </div> 

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Add Product</button>
    </form>
</div>

<style>
.import-li{
	color:#264a9c;
	background-color:white;}
.import-li .hover1a{
	display:none;}
.import-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>