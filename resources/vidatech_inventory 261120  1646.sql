-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2020 at 09:46 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_product`
--

CREATE TABLE `addon_product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `average_cost` decimal(8,2) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `expired_date` varchar(255) DEFAULT NULL,
  `maintenance` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addon_product`
--

INSERT INTO `addon_product` (`id`, `uid`, `product_name`, `category`, `product_code`, `quantity`, `part_number`, `brand`, `cost`, `average_cost`, `location`, `duration`, `expired_date`, `maintenance`, `date_created`, `date_updated`) VALUES
(1, 'b58f0ceebe4e69bcba71131d0d1d6451', 'item1', 'CAT A', 'item1pc', '8850', 'item1pn', 'item1id', '10500', '1.19', NULL, '10 days', '2020-12-06', NULL, '2020-11-26 08:41:38', '2020-11-26 08:41:38'),
(2, '119cd3ae850ebe47989822f5fa247c7b', 'item2', 'CAT B', 'item2pc', '9500', 'item2pn', 'item2id', '22500', '2.37', NULL, '15 days', '2020-12-11', NULL, '2020-11-26 08:42:51', '2020-11-26 08:42:51'),
(3, '0ed350dd5dc276a2f4a0c4410b0507d5', 'item3', 'CAT B', 'item3pc', '9800', 'item3pn', 'item3id', '2500', '0.26', NULL, '15 days', '2020-12-11', NULL, '2020-11-26 08:43:15', '2020-11-26 08:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `uid`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, '9a38ebe2ac7e40d8a1aa30e4857c6adb', 'CAT A', 'Available', '2020-07-09 08:46:46', '2020-07-09 08:46:46'),
(2, '131e9989c01e4a7a6bee19b41697e59d', 'CAT B', 'Available', '2020-07-09 08:46:51', '2020-07-09 08:46:51'),
(3, 'c10e1f2b98ec38d5a994ba9e9a6e4039', 'CAT C', 'Available', '2020-07-09 08:46:57', '2020-07-09 08:46:57');

-- --------------------------------------------------------

--
-- Table structure for table `distribution_list`
--

CREATE TABLE `distribution_list` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `quantity_received` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `average_cost` decimal(8,2) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `excel`
--

CREATE TABLE `excel` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `name`
--

CREATE TABLE `name` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `expired_date` varchar(255) DEFAULT NULL,
  `maintenance` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `product_name`, `category`, `product_code`, `quantity`, `part_number`, `brand`, `description`, `image`, `cost`, `location`, `duration`, `expired_date`, `maintenance`, `date_created`, `date_updated`) VALUES
(1, '03e525441a7bf12cdcd52f9daae979dc', 'item1', 'CAT A', 'item1pc', '8850', 'item1pn', NULL, 'item1id', '1606380098', '10500', 'p1', NULL, NULL, NULL, '2020-11-26 08:41:38', '2020-11-26 08:41:38'),
(2, 'fcfe9bc056dc19c8491783cb832ed954', 'item2', 'CAT B', 'item2pc', '9500', 'item2pn', NULL, 'item2id', '1606380171', '22500', 'p2', NULL, NULL, NULL, '2020-11-26 08:42:51', '2020-11-26 08:42:51'),
(3, '0cbb34489f45363734a3c976a09102b0', 'item3', 'CAT B', 'item3pc', '9800', 'item3pn', NULL, 'item3id', '1606380195', '2500', 'p3', NULL, NULL, NULL, '2020-11-26 08:43:15', '2020-11-26 08:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `renew_timeline`
--

CREATE TABLE `renew_timeline` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `previous_exdate` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `expired_date` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '12351236', 1, 0, '2020-01-14 06:36:33', '2020-01-14 08:28:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_product`
--
ALTER TABLE `addon_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distribution_list`
--
ALTER TABLE `distribution_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel`
--
ALTER TABLE `excel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `name`
--
ALTER TABLE `name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renew_timeline`
--
ALTER TABLE `renew_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_product`
--
ALTER TABLE `addon_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `distribution_list`
--
ALTER TABLE `distribution_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `excel`
--
ALTER TABLE `excel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `name`
--
ALTER TABLE `name`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `renew_timeline`
--
ALTER TABLE `renew_timeline`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
