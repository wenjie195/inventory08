-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2020 at 08:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_product`
--

CREATE TABLE `addon_product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `average_cost` decimal(8,2) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addon_product`
--

INSERT INTO `addon_product` (`id`, `uid`, `product_name`, `category`, `product_code`, `quantity`, `part_number`, `brand`, `cost`, `average_cost`, `location`, `date_created`, `date_updated`) VALUES
(1, 'b5688dfbdc09e6967289a17905ccce56', 'Bio C Plus (60 tabs)', 'Nutrilite', 'BCP60T', '100', 'nbc60t', 'vitamin c', '11920', '119.20', NULL, '2020-07-09 04:07:40', '2020-07-09 04:07:40'),
(2, '265e01725cb4a3e0714ae3161cfd4ff9', 'Cal Mag D', 'Nutrilite', 'CMD180T', '200', 'ncmd180t', 'vitamin d', '16800', '84.00', NULL, '2020-07-09 04:11:20', '2020-07-09 04:11:20'),
(3, '0f845fbcc4a421716e643fbce926c6da', 'Bio C Plus (60 tabs)', 'Nutrilite', 'BCP60T', '100', 'nbc60t', 'vitamin c, anti aging', '25500', '85.00', NULL, '2020-07-09 04:45:13', '2020-07-09 04:45:13'),
(4, 'af382e893b4f78683805c91292a1d859', 'Cal Mag D', 'Nutrilite', 'CMD180T', '90', 'ncmd180t', 'vitamin d', '15800', '54.48', NULL, '2020-07-09 05:01:59', '2020-07-09 05:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `uid`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'a8395296607e1f224d02eb5f188fa49f', 'Beauty', 'Available', '2020-07-08 07:35:45', '2020-07-08 07:35:45'),
(2, '2ffaac02cb0f545d022c94b992d98631', 'Home Care', 'Available', '2020-07-08 07:36:03', '2020-07-08 07:36:03'),
(3, '6575cae06c671c54f9d11a72c7a86f61', 'Home Tech', 'Available', '2020-07-08 07:36:10', '2020-07-08 07:36:10'),
(4, '92de01f7e099c0fea8a3b579e1009805', 'Nutrilite', 'Available', '2020-07-08 07:36:19', '2020-07-08 07:36:19'),
(5, '20e0db85f6eaab9f5b600fc405c8e157', 'Personal Care', 'Available', '2020-07-08 07:36:25', '2020-07-08 07:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `excel`
--

CREATE TABLE `excel` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `excel`
--

INSERT INTO `excel` (`id`, `uid`, `product_name`, `category`, `product_code`, `quantity`, `part_number`, `brand`, `description`, `cost`, `status`, `date_created`, `date_updated`) VALUES
(1, 'f8fa9d4a11d065fff9f75dca1f2bf5de', 'eSpring', 'Home Tech', 'espringwts', '100', '100', NULL, NULL, NULL, 'Pending', '2020-07-09 06:57:13', '2020-07-09 06:57:13'),
(2, '33bed5d40c6c890efa0e4120e1eba2fc', 'Atmosphere Sky', 'Home Tech', 'atmsky', '50', '50', NULL, NULL, NULL, 'Pending', '2020-07-09 06:57:13', '2020-07-09 06:57:13'),
(3, '0e6ce3db9f7c1915d02437d0f0c87b0e', 'Glister Mouthwash', 'Personal Care', 'glistermw', '400', '400', NULL, NULL, NULL, 'Pending', '2020-07-09 06:57:13', '2020-07-09 06:57:13'),
(4, '8ac665ae67a7b92043fe6e1198783f96', 'Bio C Plus (60 tabs)', 'Nutrilite', 'BCP60T', '500', '500', NULL, NULL, NULL, 'Pending', '2020-07-09 06:57:13', '2020-07-09 06:57:13'),
(5, '55f728e9656cfb9187ec5978ca541067', 'Cal Mag D', 'Nutrilite', 'CMD180T', '30', '30', NULL, NULL, NULL, 'Pending', '2020-07-09 06:57:13', '2020-07-09 06:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `name`
--

CREATE TABLE `name` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `name`
--

INSERT INTO `name` (`id`, `uid`, `name`, `product_code`, `status`, `date_created`, `date_updated`) VALUES
(1, '9d0b61b1a678f82910e0d2fdc1b923de', 'Bio C Plus', 'BCP001', 'Available', '2020-07-08 07:37:36', '2020-07-08 07:37:36'),
(2, 'de97223a058d923a5016f1e51750b44a', 'Cal Mag D', 'CMD002', 'Available', '2020-07-08 07:38:01', '2020-07-08 07:38:01'),
(3, 'b5ec0b984a501b9dbdded118e50b9e3f', 'Lecithin E', 'LE002', 'Available', '2020-07-08 07:38:11', '2020-07-08 07:38:11'),
(4, '2a0bdb4de8434da73779605227adce3f', 'eSpring', 'EWT001', 'Available', '2020-07-08 07:38:41', '2020-07-08 07:38:41'),
(5, '2daa73ac2baf09a25e2623171e5cc7ac', 'Atmosphere Sky', 'AMS001', 'Available', '2020-07-08 07:39:01', '2020-07-08 07:39:01'),
(6, '13c9d8f09655ba8915f71f2cea1401bb', 'Atmosphere Drive', 'AMS002', 'Available', '2020-07-08 07:39:17', '2020-07-08 07:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `product_name`, `category`, `product_code`, `quantity`, `part_number`, `brand`, `description`, `image`, `cost`, `location`, `date_created`, `date_updated`) VALUES
(1, 'b5688dfbdc09e6967289a17905ccce56', 'Bio C Plus (60 tabs)', 'Nutrilite', 'BCP60T', '300', 'nbc60t', NULL, 'vitamin c, anti aging', '1594267660BC.jpg', '25500', 'nutrilite section', '2020-07-09 04:07:40', '2020-07-09 04:07:40'),
(2, '265e01725cb4a3e0714ae3161cfd4ff9', 'Cal Mag D', 'Nutrilite', 'CMD180T', '290', 'ncmd180t', NULL, 'vitamin d', '1594267880D.jpg', '15800', 'nutrilite section', '2020-07-09 04:11:20', '2020-07-09 04:11:20');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '12351236', 1, 0, '2020-01-14 06:36:33', '2020-01-14 08:28:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_product`
--
ALTER TABLE `addon_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel`
--
ALTER TABLE `excel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `name`
--
ALTER TABLE `name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_product`
--
ALTER TABLE `addon_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `excel`
--
ALTER TABLE `excel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `name`
--
ALTER TABLE `name`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
