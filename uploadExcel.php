<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

include 'selectFilecss.php';
include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploadsExcel/'.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $productName = "";
        if(isset($Row[0])) 
        {
          $productName = mysqli_real_escape_string($conn,$Row[0]);
        }
        $category = "";
        if(isset($Row[0])) 
        {
          $category = mysqli_real_escape_string($conn,$Row[1]);
        }
        $productCode = "";
        if(isset($Row[0])) 
        {
          $productCode = mysqli_real_escape_string($conn,$Row[2]);
        }
        $quantity = "";
        if(isset($Row[0])) 
        {
          $quantity = mysqli_real_escape_string($conn,$Row[3]);
        }
        $partNumber = "";
        if(isset($Row[0])) 
        {
          $partNumber = mysqli_real_escape_string($conn,$Row[4]);
        }

        //generate unique uid
        $uid = md5(uniqid());

        $status ="Pending";

        if (!empty($productName) || !empty($category) || !empty($productCode) || !empty($quantity) || !empty($partNumber))
        {
          $query = "INSERT INTO excel (product_name,category,product_code,quantity,part_number,uid,status) VALUES ('".$productName."','".$category."','".$productCode."','".$quantity."','".$partNumber."','".$uid."','".$status."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // echo "<script>alert('Excel Uploaded !');window.location='../inventory/uploadExcel.php'</script>";         
            echo "<script>alert('Excel Uploaded !');window.location='../inventory08/adminViewOrderDetails.php'</script>";   
          }
          else 
          {
            echo "<script>alert('Problem in Importing Excel Data !');window.location='../inventory08/uploadExcel.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../inventory08/uploadExcel.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Import Data | ChiNou IMS" />
  <title>Import Data | ChiNou IMS</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar"> 

<h1 class="h1-title">Import Excel File</h1>

  <div class="outer-container">
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

<style>
  .import-li{
  color:#264a9c;
  background-color:white;}
  .import-li .hover1a{
  display:none;}
  .import-li .hover1b{
  display:block;}
</style>

<?php include 'js.php'; ?>
<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>

<script>
  $(function() {
    $( '#dl-menu' ).dlmenu({
      animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
    });
  });
</script>

</body>
</html>