<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Excel.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $requestProductUid = rewrite($_POST['product_uid']);
    $requestProductName = rewrite($_POST['product_name']);
    $requestProductCode = rewrite($_POST['product_code']);
    $requestProductQuantity = rewrite($_POST['product_quantity']);

    $conn->close();
}

// $conn = connDB();

// $productDetails = getExcel($conn, "WHERE status = 'Pending' ");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="BOM List Details| ChiNou IMS" />
    <title>BOM List Details| ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Product Name : <?php echo $_POST['product_name']; ?>
        </a>
    </h1>

    <?php
    if(isset($_POST['product_name']))
    {
    $conn = connDB();
    // $productDetails = getProduct($conn,"WHERE product_name = ? ", array("product_name") ,array($requestProductName),"s");
    $productDetails = getProduct($conn,"WHERE product_name = ? AND product_code = ? ", array("product_name","product_code") ,array($requestProductName,$requestProductCode),"ss");
    ?>
        <table class="shipping-table">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Product Name</th>
                    <!-- <th>Category</th>
                    <th>Product Code</th>
                    <th>Part Number</th> -->
                    <th>Current Quantity</th>
                    <th>Requested Quantity</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productName = $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $currentQuantity = $productDetails[$cnt]->getQuantity();?></td>
                            <td>
                                <?php 
                                    echo $requestProductQuantity;
                                    if($requestProductQuantity > $currentQuantity)
                                    {
                                        echo "<br>";
                                        echo "Requested Quantity higher than current quantity, please restock !!";
                                    }
                                    else
                                    {}
                                ?>
                            </td>

                            <?php $productCode = $productDetails[$cnt]->getProductCode();?>
                            <?php $productQuantity = $productDetails[$cnt]->getQuantity();?>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                else
                {
                    echo "No Data Found !!";
                }
                ?>
            </tbody>
        </table>

        <?php
        if($currentQuantity > $requestProductQuantity)
        {
        ?>
        
            <form action="utilities/distributionFunction.php" method="POST">
                <h1 class="h1-title">Distribution Form</h1> 

                <div class="input50-div">
                    <p class="input-title-p">Name</p>
                    <input class="clean tele-input" type="text" placeholder="Name" id="name" name="name" required>        
                </div> 

                <div class="input50-div second-input50">
                    <p class="input-title-p">Department</p>
                    <input class="clean tele-input" type="text" placeholder="Department" id="department" name="department" required>        
                </div> 

                <div class="clear"></div>

                <div class="input50-div">
                    <p class="input-title-p">Quantity Provided</p>
                    <input class="clean tele-input" type="text" placeholder="Quantity Provided" id="quantity" name="quantity" required>        
                </div> 

                <input class="clean tele-input" type="hidden" value="<?php echo $requestProductUid;?>" id="product_order_uid" name="product_order_uid" readonly> 
                <input class="clean tele-input" type="hidden" value="<?php echo $productName;?>" id="product_name" name="product_name" readonly>  
                <input class="clean tele-input" type="hidden" value="<?php echo $productCode;?>" id="product_code" name="product_code" readonly>  
                <input class="clean tele-input" type="hidden" value="<?php echo $productQuantity;?>" id="current_quantity" name="current_quantity" readonly> 
                
                <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>
            </form>

        <?php
        }
        else
        {}
        ?>

    <?php
    }
    ?>

</div>

<style>
.customer-li{
	color:#264a9c;
	background-color:white;}
.customer-li .hover1a{
	display:none;}
.customer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>