<div class="side-bar red-bg no-print">
	<div class="width100 text-center">
    	<img src="img/logo.png" class="logo-img" alt="logo" title="logo">
    </div>
	<ul class="sidebar-ul">
    	<a href="adminDashboard.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>
    	<a href="adminAddProduct.php">
        	<li class="sidebar-li account-li hover1">
            	<img src="img/add-product.png" class="hover1a" alt="Add Product" title="Add Product">
                <img src="img/add-product2.png" class="hover1b" alt="Add Product" title="Add Product">
                 <p>Add Product</p>
            </li>
        </a>
        <a href="adminAddCategory.php">
        	<li class="sidebar-li telemarketer-li hover1">
            	<img src="img/category.png" class="hover1a" alt="Add New Category" title="Add New Category">
                <img src="img/category2.png" class="hover1b" alt="Add New Category" title="Add New Category">
                 <p>Add New Category</p>
            </li>
        </a>
        <a href="uploadExcel.php">
        	<li class="sidebar-li import-li hover1">
            	<img src="img/import-data1.png" class="hover1a" alt="Import BOM List" title="Import BOM List">
                <img src="img/import-data2.png" class="hover1b" alt="Import BOM List" title="Import BOM List">            
             	 <p>Import BOM List</p>
            </li>
        </a>
        <a href="adminViewOrderDetails.php">
        	<li class="sidebar-li viewbom-li hover1">
            	<img src="img/edit-profile2.png" class="hover1a" alt="View BOM List" title="View BOM List">
                <img src="img/edit-profile.png" class="hover1b" alt="View BOM List" title="View BOM List">
                 <p>View BOM List</p>
            </li>
        </a>
        <a href="adminPreInovice.php">
        	<li class="sidebar-li invoice-li hover1">
            	<img src="img/invoice.png" class="hover1a" alt="Invoice" title="Invoice">
                <img src="img/invoice2.png" class="hover1b" alt="Invoice" title="Invoice">
                 <p>Invoice</p>
            </li>
        </a>
        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
    </ul>
</div>

<header id="header" class="header header--fixed same-padding header1 menu-color no-print" role="banner">
        <img src="img/logo.png" class="mobile-logo" alt="Logo" title="Logo">
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <a href="adminDashboard.php">
                    <li class="sidebar-li dashboard-li hover1">
                        <img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                         <p>Dashboard</p>
                    </li>
                </a>
                <a href="adminAddProduct.php">
                    <li class="sidebar-li account-li hover1">
                        <img src="img/add-product.png" class="hover1a" alt="Add Product" title="Add Product">
                        <img src="img/add-product2.png" class="hover1b" alt="Add Product" title="Add Product">
                         <p>Add Product</p>
                    </li>
                </a>
                <a href="adminAddCategory.php">
                    <li class="sidebar-li telemarketer-li hover1">
                        <img src="img/category.png" class="hover1a" alt="Add New Category" title="Add New Category">
                        <img src="img/category2.png" class="hover1b" alt="Add New Category" title="Add New Category">
                         <p>Add New Category</p>
                    </li>
                </a>
                <a href="uploadExcel.php">
                    <li class="sidebar-li import-li hover1">
                        <img src="img/import-data1.png" class="hover1a" alt="Import Data" title="Import Data">
                        <img src="img/import-data2.png" class="hover1b" alt="Import Data" title="Import Data">            
                         <p>Import BOM List</p>
                    </li>
                </a>
                <a href="adminViewOrderDetails.php">
                    <li class="sidebar-li viewbom-li hover1">
                        <img src="img/edit-profile2.png" class="hover1a" alt="View BOM List" title="View BOM List">
                        <img src="img/edit-profile.png" class="hover1b" alt="View BOM List" title="View BOM List">
                         <p>View BOM List</p>
                    </li>
                </a>
                <a href="adminPreInovice.php">
                    <li class="sidebar-li invoice-li hover1">
                        <img src="img/invoice.png" class="hover1a" alt="Invoice" title="Invoice">
                        <img src="img/invoice2.png" class="hover1b" alt="Invoice" title="Invoice">
                         <p>Invoice</p>
                    </li>
                </a>              
                <a href="logout.php">
                    <li class="sidebar-li hover1">
                        <img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                        <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
                         <p>Logout</p>
                    </li>
               </a>
            </ul>
    </div>
</header>