<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard| ChiNou IMS" />
    <title>Admin Dashboard| ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	Product Name : <?php echo $_POST['product_name']; ?>
        </a>
    </h1>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">

                <?php
                    if(isset($_POST['product_name']))
                    {
                        $conn = connDB();
                        $productDetails = getAddOnProduct($conn,"WHERE product_name = ? ORDER BY date_updated DESC ", array("product_name") ,array($_POST['product_name']),"s");
                    ?>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Product Code</th>
                                <th>Part Number</th>
                                <th>Quantity</th>
                                <th>Cost</th>
                                <th>Average Cost</th>
                                <th>Expired Date</th>
                                <th>Maintenance Status</th>
                                <th>Date Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if($productDetails)
                            {   
                                for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                                {
                                ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $productDetails[$cnt]->getProductCode();?></td>
                                    <td><?php echo $productDetails[$cnt]->getPartNumber();?></td>
                                    <td><?php echo $productDetails[$cnt]->getQuantity();?></td>
                                    <td><?php echo $productDetails[$cnt]->getCost();?></td>
                                    <td><?php echo $productDetails[$cnt]->getAverageCost();?></td>
                                    <td><?php echo $expiredDate = $productDetails[$cnt]->getExpiredDate();?></td>

                                    <td>
                                        <?php
                                            $tz = 'Asia/Kuala_Lumpur';
                                            $timestamp = time();
                                            $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
                                            $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
                                            $currentTime = $dt->format('Y-m-d');

                                            // $currentTime = "2020-11-01" ;

                                            $manintenanceStatus = $productDetails[$cnt]->getMaintenance();

                                            if($currentTime >= $expiredDate)
                                            // if($currentTime >= $expiredDate && $manintenanceStatus == "")
                                            // if($currentTime >= $expiredDate && $manintenanceStatus != "Done")
                                            {
                                                echo "Alert";
                                            }
                                            else
                                            {}
                                        ?>
                                    </td>

                                    <td><?php echo $productDetails[$cnt]->getDateCreated();?></td>

                                    <td>
                                        <form action="adminUpdateProductDuration.php" method="POST">
                                            <button class="clean hover1 img-btn" type="submit" name="product_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                                <img src="img/edit2.png" class="width100 hover1a" alt="Update" title="Update">
                                                <img src="img/edit3.png" class="width100 hover1b" alt="Update" title="Update">
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                                <?php
                                }
                                ?>
                            <?php
                            }
                            ?>
                        </tbody>
                    <?php
                    }
                ?>

                </table>
            </div>
    </div>

    <div class="clear"></div>

</div>

<style>
.customer-li{
	color:#264a9c;
	background-color:white;}
.customer-li .hover1a{
	display:none;}
.customer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>