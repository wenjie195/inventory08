<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getProduct($conn);
$allCategory = getCategory($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Update Duration | ChiNou IMS" />
    <title>Admin Update Duration | ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <!-- <form method="POST" action="utilities/editProductFunction.php"> -->
    <form method="POST" action="utilities/updateProductDurationFunction.php">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Product ID : <?php echo $_POST['product_uid']; ?>
        </a>
    </h1>

    <?php
    if(isset($_POST['product_uid']))
    {
    $conn = connDB();
    $productDetails = getAddOnProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['product_uid']),"s");
    ?>

        <div class="input50-div">
            <p class="input-title-p">Product Name</p>   
            <input class="clean tele-input" type="text" placeholder="Product Name" value="<?php echo $productDetails[0]->getProductName();?>" readonly>
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Duration (Format : 10 days / 5 months / 2 years)</p>
            <input class="clean tele-input"  type="text" placeholder="Duration" id="duration" name="duration" required>   
        </div> 

        <input class="clean tele-input" type="hidden" value="<?php echo $productDetails[0]->getUid();?>" id="product_uid" name="product_uid" readonly>    

    <?php
    }
    ?>

    <div class="clear"></div>

    <button class="clean red-btn margin-top30 fix300-btn" name="submit">Update</button>

    </form>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>