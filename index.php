<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Login | ChiNou IMS" />
    <title>Login | ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="width100 overflow">

	<div class="width50 grey-bg right-grey-div">
    	<!--<img src="img/logo2.png" class="index-logo" alt="ChiNou IMS" title="ChiNou IMS">-->
    	<h1 class="tele-h1">Login</h1>
        <p class="login-p margin-bottom">
        	Please enter your ID and password to login.
        </p>
         <form  class="tele-form" action="utilities/loginFunction.php" method="POST">
         	<input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
         	<input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            <button class="clean red-btn"name="loginButton">Login</button>
         </form>
    </div>
	<div class="width50 left-red-div red-bg">
    	<img src="img/logo.png" class="login-img" alt="Login" title="Login">
    </div>    
</div>

<?php include 'js.php'; ?>
</body>
</html>