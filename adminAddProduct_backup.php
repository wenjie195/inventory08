<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard| ChiNou IMS" />
    <title>Admin Dashboard| ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <form method="POST" action="utilities/addOnProductFunction.php" enctype="multipart/form-data">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Product ID : <?php echo $_POST['product_uid']; ?>
        </a>
    </h1>

    <?php
    if(isset($_POST['product_uid']))
    {
    $conn = connDB();
    $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['product_uid']),"s");
    ?>

        <div class="input50-div">
            <p class="input-title-p">Product Name</p>   
            <input class="clean tele-input" type="text" value="<?php echo $productDetails[0]->getProductName();?>" id="product_name" name="product_name" readonly>
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Product Code</p>
            <input class="clean tele-input" type="text" value="<?php echo $productDetails[0]->getProductCode();?>" id="product_code" name="product_code" readonly>    
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Quantity</p>
            <input class="clean tele-input" type="text" placeholder="Quantity" id="update_quantity" name="update_quantity" required>
        </div>

        <div class="input50-div second-input50">
            <p class="input-title-p">Cost</p>
            <input class="clean tele-input" type="text" placeholder="Cost" id="update_cost" name="update_cost" required>    
        </div> 

        <div class="input50-div">
            <p class="input-title-p">Repoint Order</p>
            <input class="clean tele-input" type="text" placeholder="Repoint Order" id="repoint_order" name="repoint_order" required>
        </div>

        <div class="input50-div second-input50">
            <p class="input-title-p">Location</p>
            <input class="clean tele-input" type="text" placeholder="Location" id="location" name="location" required>    
        </div> 

        <input class="clean tele-input" type="hidden" value="<?php echo $productDetails[0]->getUid();?>" id="product_uid" name="product_uid" readonly>    

    <?php
    }
    ?>

    <div class="clear"></div>

    <button class="clean red-btn margin-top30 fix300-btn" name="submit">Add Product</button>

    </form>

</div>

<style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

<script>
    function random_function()
    {
        var input = document.getElementById("update_type");

        if(update_type="Good")
        {
        var arr = document.getElementById("Good");
        arr.style.display = input.value == "Good" ? "block" : "none";
        }

        if(update_type="Bad")
        {
        var arr = document.getElementById("Bad");
        arr.style.display = input.value == "Bad" ? "block" : "none";
        }

        if(update_type="Other")
        {
        var arr = document.getElementById("Other");
        arr.style.display = input.value == "Other" ? "block" : "none";
        }

        else(update_type="No Take Call")
        {
        var arr = document.getElementById("No Take Call");
        arr.style.display = input.value == "No Take Call" ? "block" : "none";
        }
    }
</script>

</body>
</html>