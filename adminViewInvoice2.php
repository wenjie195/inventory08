<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');

// echo $dateAdded = $productDetails[$cnt]->getDateCreated();
// echo "<br>";
$Date1 = $time;
$date = new DateTime($Date1);
// $additional = $duration;
$additional = "-3 hour";
// $date->modify('+1 day');
$date->modify($additional);
$expiredDate = $date->format('Y-m-d H:i:s');

$conn = connDB();

// $productDetails = getProduct($conn);
// $productDetails = getProduct($conn, "WHERE date_created > '$expiredDate' ");
$productDetails = getAddOnProduct($conn, "WHERE date_created > '$expiredDate' ");

if($productDetails)
{
    $totalBonusOne = 0;
    for ($cnt=0; $cnt <count($productDetails) ; $cnt++)
    {
    $totalBonusOne += $productDetails[$cnt]->getCost();
    }
}
else
{
    $totalBonusOne = 0 ;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $registerMS = rewrite($_POST["register_ms"]);
    $registerAttention = rewrite($_POST["register_attention"]);
}
else 
{   }

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard | ChiNou IMS" />
    <title>Admin Dashboard | ChiNou IMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h3 class="h1-title open">VIDATECH (M) SDN BHD</h3>
    <h3 class="h1-title open">Company No. : 1160315-W</h3>

    <div class="input50-div">
      <p class="input-title-p">M/s : <?php echo $registerMS;?></p>
    </div> 

    <div class="clear"></div>

    <!-- <div class="input50-div second-input50"> -->
    <div class="input50-div">
      <p class="input-title-p">Attention : <?php echo $registerAttention;?></p>
    </div>

    <div class="clear"></div>

    <h2 class="h1-title open">Invoice</h2>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Product Code</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productName = $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $productDetails[$cnt]->getCategory();?></td>
                            <td><?php echo $productDetails[$cnt]->getProductCode();?></td>
                            <td><?php echo $quantity = $productDetails[$cnt]->getQuantity();?></td>
                            <td>
                              <?php 
                                $price = $productDetails[$cnt]->getCost();
                                echo $totalPrice =  $price / $quantity;
                              ?>
                            </td>
                            <td>
                              <?php echo $price;?>
                            </td>
                        </tr>

                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>

        <div class="clear"></div>

        <h3 class="h1-title open">Grand Total : <?php echo $totalBonusOne;?></h3>

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn" onclick="window.print()">Generate PDF</button>

    </div>
    
    <div class="clear"></div>

</div>

<style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>